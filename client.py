#!/bin/python3

'''
Title: GTK Socket Chat
Author: Lukas Kasticky
Date: 2020-05-25
License: Unlicense
Description: GTK Socket Chat (Client)
'''

import gi
import pickle
import select
import socket
import sys
from threading import Thread, Lock

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

IP = "127.0.0.1"
PORT = 5050

# api functions for connecting to server
class API:

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # connect to server
    def connect(self, ip = IP, port = PORT):
        self.ip = ip
        self.port = port

        try:
            self.s.connect((ip, port))
            builder.get_object("status").set_text("Connected to {}:{}".format(ip, port))
        except Exception as e:
            print(e)
            builder.get_object("status").set_text("Connection refused")

    def msg_send(self, sender, msg):
        try:
            data = pickle.dumps([sender, msg], 4)
            self.s.send(data)
        except Exception:
            builder.get_object("status").set_text("Connection lost")

    def msg_recv(self):
        while True:
            # Receive messages
            data = self.s.recv(1024)

            # Break if no data has been received
            if not data:
                break

            # Decode data
            data = pickle.loads(data)
            user = data[0]
            msg = data[1]

            if (user == ""):
                user = "Unnamed User"

            if (msg != ""):
                GLib.idle_add(update_rows, user, msg)

            # Log message in console
            print("[MSG][{}] {}".format(user, msg))

def update_rows(user, msg):
    # Create row
    row = Gtk.ListBoxRow.new()
    row.set_focus_on_click(False)
    
    # Create vbox
    vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 2)
    vbox.set_margin_top(6)
    vbox.set_margin_start(6)
    vbox.set_margin_end(6)
    vbox.set_margin_bottom(6)

    # Create GTK labels
    label_msg = Gtk.Label.new(msg)
    label_user = Gtk.Label.new(user)
    label_msg.set_xalign(0)
    label_user.set_xalign(0)
    label_user.get_style_context().add_class("dim-label")
    
    # Pack labels into box into list
    vbox.add(label_msg)
    vbox.add(label_user)
    row.add(vbox)

    # Insert row into `GtkListBox`
    msg_list = builder.get_object("msg_list")
    msg_list.add(row)
    row.show_all()

    return False

# gtk signal handler
class SignalHandler:
    # runs when window is closed
    def on_destroy(self, *args):
        # t.kill() # doesn't exist
        Gtk.main_quit()

    # runs when send button is pressed
    def on_send(self, *args):
        field_name = builder.get_object("field_name")
        field_msg = builder.get_object("field_msg")
        api.msg_send(field_name.get_text(), field_msg.get_text())
        field_msg.set_text("")

if __name__ == "__main__":
    # load glade file
    builder = Gtk.Builder()
    builder.add_from_file("app.glade")
    builder.connect_signals(SignalHandler())

    # show main window
    app = builder.get_object("main_window")
    app.show_all()

    # connect to server
    api = API()
    api.connect()

    # start receiving messages in second thread
    t = Thread(target = api.msg_recv)
    t.daemon = True
    t.start()

    # main
    Gtk.main()
