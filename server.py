#!/bin/python3

'''
Title: GTK Socket Chat
Author: Lukas Kasticky
Date: 2020-05-25
License: Unlicense
Description: GTK Socket Chat (Server)
'''

import pickle
import socket
# from multiprocessing import Lock, Process, Value
from threading import Lock, Thread

IP = "" # allow connections from all hosts
PORT = 5050

class Client(Thread):
    def __init__(self, ip, port, conn, lock):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.conn = conn
        self.lock = lock
        print("[+] Client `{}:{}` connected".format(ip, port))

    def run(self):
        try:
            while True:
                # Receive messages
                data = conn.recv(1024)
                
                # Break if no data has been received
                if not data:
                    print("[-] Client `{}:{}` disconnected".format(self.ip, self.port))
                    break

                # Send message to all clients
                with self.lock:
                    for c in clients:
                        c.conn.sendall(data)

                # Log message in console
                data = pickle.loads(data)
                user = data[0]
                msg = data[1]
                print("[{}:{}][{}] {}".format(self.ip, self.port, user, msg))
        finally:
            with self.lock:
                clients.remove(self)
                # self.close()
                self.join()

    def send(self, data):
        conn.send(data)

if __name__ == "__main__":
    clients = set()
    lock = Lock()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.bind((IP, PORT))

        while True:
            # Wait for new connections
            s.listen(5)
            (conn, (ip, port)) = s.accept()

            # create new client
            c = Client(ip, port, conn, lock)
            c.start()
            clients.add(c)
    except Exception as e:
            print(e)
    finally:
        s.close()
        for c in clients:
            c.join()
            c.close()
