# GTK Socket Chat

This is just a simple demo app for Python + Sockets + GTK

## Screenshots

![Empty State](Screenshot_Empty.png)

![Chat](Screenshot_Chat.png)

## Usage

Follow the getting started guide from [here](https://pygobject.readthedocs.io/en/latest/getting_started.html) to install GTK

### Server

```
python3 server.py
```

### Client

```
python3 client.py
```
